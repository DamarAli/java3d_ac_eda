package iti.java3d.dao;

import java.sql.*;
import java.util.*;


public class JDBCSimple1 {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			
			DAO dao = new DAO();
			
		try {
			Connection connect = dao.get_connection();
			
			//Nécessaire pour executer des requêtes
			// Utilisation de la librairie java.sql.Statement
			Statement stmt = connect.createStatement();
			
			//Chaine de caractère écrit en dure
			String req1= "INSERT INTO Balneo_Facture VALUES (7,to_date('17/10/2018','yyyy/mm/dd'),120);";
			//Execution de la requête "req1"
			//Retourne un entier (res) image du nombre de lignes impactées
			int res = stmt.executeUpdate(req1); //Pour inserer : executeUpdate
			
			
			// ExÃ©cution dâ€™une requÃªte de sÃ©lection (SELECT)
			String req2 = "SELECT * FROM Balneo_Facture";
			ResultSet result = stmt.executeQuery(req2); //Pour afficher : executeQuery
			
			//Affichage des données grâce au curseur next()
			//Cette méthode renvoie true si le curseur s’est déplacée et false si le curseur n’a plus de lignes à explorer
			while (result.next()) {
				System.out.println("fact_no : " + result.getInt(1));
				System.out.println("fact_date : " + result.getString(2));
				System.out.println("fact_montant : " + result.getInt(3));

			}
			
		}
		
		catch (Exception e) {
			e.printStackTrace();
		}
			
			
		}

}
